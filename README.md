# fframework

## What?
Continuous compilation and validation of your front-end code; just `grunt` it.

- `grunt` - runs the default (watch) task
- `grunt watch` - the default task (watch it all)
- `grunt watch:js` - only watch for js changes

Once started, the default build will run whenever files in the project change.  

Have a look at the [Gruntfile](build/Gruntfile.js) that configures all of this if you're feeling fruity!


## Lint
Javascript is linted using [JSHint](http://jshint.com/install/). See `.jshintrc` for specifics on what is linted. Javascript libraries such as jQuery plugins are ignored.
Please ensure these are added to the `libs` folder. Javascript will fail to compile unless all files are error free.

Sass is linted using [scsslint](https://github.com/brigade/scss-lint/). See `scss-lint.yml` for specifics on what is linted. Libraries are ignored.
Sass will fail to compile unless all files are error free

## Test
JavaScript unit tests are completed using [QUnit](https://qunitjs.com/) and phantomJS.

Sass isn't currently unit tested.

---

## System Requirements
- [Ruby](https://www.ruby-lang.org/en/installation/)
- [Ruby Bundler](http://bundler.io/)
- [NodeJS / NPM](http://nodejs.org/)
- [Grunt CLI](http://gruntjs.com/getting-started/)

NPM and Grunt will take care of the rest. Firstly by installing Grunt, then utilising Grunt to fetch the other dependencies.


## Getting Started  

`$ cd <%path to checkout%>/build`  
Go to the build folder

`$ npm install`  
Install all of the packages required by Grunt.

---

## Whom?
* [Gareth](https://twitter.com/garethmead) - concept, html, sass
* [Mike](https://twitter.com/mikedotalmond) - js / build
