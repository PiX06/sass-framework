define(['jquery'], function (jq) {
    return jq.noConflict( window.globaljQuery === false );
});