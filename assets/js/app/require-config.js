/* jshint unused:false */

/*doc
---
title: Require Config
name: app/require-config
category: JS - app
---
[`app/require-config`](../../assets/js/app/require-config.js)

requirejs application configuration


*/
var globaljQuery = false;
// Setting globaljQuery to `true` will expose 'jQuery' on the window object - using .noConflict(false), does not expose as '$'.
// Can be useful for libs / 3rd party stuff that *needs* access to a window.jQuery

var GreenSockGlobals = {}; // required by GSAP

require.config({

	waitSeconds: 7, // default load timeout is 7 seconds
	enforceDefine: true, // check for define/shim exports and get timely, correct, error triggers in IE.

	baseUrl:'../../',
	
	map: {
		/* https://stackoverflow.com/questions/16736663/require-js-jquery-noconflict-shim-why-is-init-method-never-called */
		// '*' means all modules will get 'jquery-private' for their 'jquery' dependency.
		'*': { 'jquery': 'jquery-private' },
		// 'jquery-private' wants the real jQuery module though.
		// If this line was not here, there would be an unresolvable cyclic dependency.
		'jquery-private': { 'jquery': 'jquery' }
	},

	paths: {
		jquery: [
			"//code.jquery.com/jquery-1.11.1.min",
			"libs/jquery/jquery-1.11.1.min"
		],
		// private/internal jquery - not available outside the app scope
		"jquery-private": "libs/jquery/jquery-private",

		// jQuery plugins & related
		parsley: "libs/jquery/utils/parsley",
		slick: "libs/jquery/utils/slick",
		uniform: "libs/jquery/utils/uniform-2.1.2",
		placeholder:'libs/jquery/utils/jquery.placeholder',
		fancybox:'libs/jquery/utils/jquery.fancybox',
        
		underscore:"libs/underscore",
        
		/* If needed, Modernizr should be included in the head, before the requirejs app. This module checks for that, and exports the global window.Modernizr */
		Modernizr:"libs/modernizr-module",
        
		/* https://github.com/millermedeiros/js-signals */
		signals: "libs/signals",

		// NOTE: This needs to be here to set up tweenlite correctly, however
		// you will usually want use the 'core/tween' module to get TweenLite with the CSS and jquery plugins enabled.
		TweenLite:"libs/greensock/TweenLite"
	},
    
	shim: {
		TweenLite: { exports:"GreenSockGlobals.TweenLite" },
		"libs/soundcloud-api": { exports:"window.SC" }
	}
});
