/*
*/
(function(){ "use strict";
	define([
		"jquery",
        "core/logger",
			
		"uniform",
		"placeholder",
		"libs/what-input"
	],
	function ($, Logger) {

		var start = function() {
			
			Logger.log('[PluginActivator] start');
			
			/**
			 * uniform setup...
			 */
			$("select, input:checkbox, input:radio").not(".nouniform").uniform({
				useID: false,
				selectAutoWidth: false
			});
			
			/**
			 * placeholder setup....
			 */
			$('input, textarea').placeholder();
		};
		
		// expose public fields
		return {
            start : start
        };
	});
})();
