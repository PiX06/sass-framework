/*
	Example / empty module template
*/

(function(){ "use strict";
	define(
	[
        "core/logger"
	],

	/* factory */
	function (Logger) {

		var 
			multiplyAB = function(a,b){
				return a * b;
			},
			
			init = function() {
				Logger.log('[Empty]::init - I\'m an empty / example module');
			},

			start = function(){
				Logger.log('[Empty]::start - I\'m an empty / example module');
			};
			
		
		// expose public fields
		return {
            init : init,
            start : start,
            multiplyAB : multiplyAB
        };
	});
})();