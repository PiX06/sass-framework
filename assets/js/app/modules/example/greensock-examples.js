/*doc
---
title: Animation
name: app/modules/example/greensock-examples
category: JS - app ~ modules ~ example
---

[`app/modules/greensock-examples`](../../assets/js/app/modules/example/greensock-examples.js)

There are many more examples, documentation, etc... on the greensock site, [get started](http://greensock.com/jump-start-js) there!


```html_example
<div id="greensock-examples">

    <h4>Animate a value - <span id="scoreDisplay"></span></h4>
    <br>
    <h4>Control</h4>
    <style>
        #logo {
            position: relative;
            width: 60px;
            height: 60px;
            background: url(//s3-us-west-2.amazonaws.com/s.cdpn.io/16327/logo_black_1.jpg) no-repeat;
        }
    </style>
    <div>  <div id="logo"></div> </div>
    <input type="button" id="playBtn" value="play()">
    <input type="button" id="pauseBtn" value="pause()">
    <input type="button" id="resumeBtn" value="resume()">
    <input type="button" id="reverseBtn" value="reverse()">
    <input type="button" id="playFromBtn" value="play(5)">
    <input type="button" id="reverseFromBtn" value="reverse(1)">
    <input type="button" id="seekBtn" value="seek(3)"><br/>
    <input type="button" id="timeScaleSlowBtn" value="timeScale(0.5)">
    <input type="button" id="timeScaleNormalBtn" value="timeScale(1)">
    <input type="button" id="timeScaleFastBtn" value="timeScale(2)">
    <input type="button" id="restartBtn" value="restart()">
</div>
```

*/

(function(){ "use strict";
	define(
	[
        "jquery",
        "core/logger",
        "core/tween"
	],

	/* factory */
	function ($, Logger, TweenLite) {

		var
            animateScore = function(){
                var demo = {score:0},
                    scoreDisplay = document.getElementById("scoreDisplay"),
                    updateValue = function() { scoreDisplay.innerHTML = demo.score.toFixed(2); };

                TweenLite.to(demo, 20, {score:100, onUpdate:updateValue});
            },

            setupControls = function() {
                var tween = TweenLite.to("#logo", 6, {left:"632px"});
                $('#playBtn').on("click", function(){ tween.play(); } );
                $('#pauseBtn').on("click", function(){ tween.pause(); } );
                $('#resumeBtn').on("click", function(){ tween.resume(); } );
                $('#reverseBtn').on("click", function(){ tween.reverse(); } );
                $('#playFromBtn').on("click", function(){ tween.play(5); } );
                $('#reverseFromBtn').on("click", function(){ tween.reverse(1); } );
                $('#seekBtn').on("click", function(){ tween.seek(3); } );
                $('#timeScaleSlowBtn').on("click", function(){ tween.timeScale(0.5); } );
                $('#timeScaleNormalBtn').on("click", function(){ tween.timeScale(1); } );
                $('#timeScaleFastBtn').on("click", function(){ tween.timeScale(2); } );
                $('#restartBtn').on("click", function(){ tween.restart(); } );
            },


			init = function() {
				Logger.log('[GreenSockExamples]::init');
			},

			start = function(){
				Logger.log('[GreenSockExamples]::start');
				Logger.log('[GreenSockExamples]::See http://greensock.com/jump-start-js to get started and see what you can do!');

                if($('#greensock-examples').length===1){
                    setupControls();
                    animateScore();
                }
			};


		// expose public fields
		return {
            init : init,
            start : start
        };
	});
})();