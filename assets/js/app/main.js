/*doc
---
title: Main
name: app/main
category: JS - app
---
[`app/main`](../../assets/js/app/main.js)

Application entry-point - should just be used to initialise and start modules. Avoid adding any application logic here.

This is just an example of the app/main.js - do take the time to customise, use only what you need, and make it your own :-)

Some more examples can be found in app/modules/example

*/
(function(window, document){ "use strict";
    require([ // Only require the modules you need / are using
        // libs
        "jquery",
        "underscore",
        "Modernizr",
        
        // core
        "core/logger",
        
        // core~util    
        "core/breakpoints",
        "core/device",
            
        // core dom/ui
        "core/equalHeights",
        "core/accordion",
        
        // core~media
        "core/media/vimeo-embed",
        "core/media/youtube-embed",
        "core/media/soundcloud-embed",
            
        // an object containing compile time constants like build-time, project name, project desc, project version, etc...
        "app/build-info",           
            
        // example modules
        "app/modules/example/empty",
        "app/modules/example/plugin-activator",
        "app/modules/example/greensock-examples",
    ],
    function($, _, Modernizr, // libs
             Logger, Breakpoints, Device, EqualHeights, Accordion, VimeoEmbed, YoutTubeEmbed, SoundcloudEmbed,// core
             BuildInfo, EmptyModule, PluginActivator, AnimExample) { // example app  modules
        
        var
            /**
             * [app/main] init
             * ---------------
             *
             * The app js is ready, but the rest of the page and DOM may not be...
             *
             *  Do      : Perform module initialisation / prepare / load data/config etc.
             *  Do not  : Rely on the DOM remaining in it's current state.
             *
             */
            init = function(){
                Logger.log("[app/main] init");
                
                Breakpoints.init();
                
                YoutTubeEmbed.init();
            },
            

            /**
             * [app/main] start
             * ----------------
             *
             * The DOM is ready, you can now start doing things that need (read/write) access to the DOM
             * NOTE: Page load may not be complete here (assets / 3rd party and async scripts, etc)
             *
             */
            start = function() {

                Logger.log("[app/main] start");
                
                Device.start();
                
                Breakpoints.start();

                PluginActivator.start();
                
                EqualHeights.start();
                
                Accordion.start();
                
                EmptyModule.start();
                
                AnimExample.start();               
                
                VimeoEmbed.start();
                
                YoutTubeEmbed.start();
                
                SoundcloudEmbed.start();

            };

            // init now
            init();
            // start when doc ready
            $(document).ready(start);
        }
    );
})();
