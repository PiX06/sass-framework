/*doc
---
title: Tween
name: core/tween
category: JS - core
---
[`core/tween`](../../assets/js/core/tween.js)

A wrapper module for a TweenLite set-up that includes the CSS and jquery.gsap plugins for the GreenSock Animation Platform (GSAP).

This is a base setup for normal day-to-day use. `CSSPlugin` and `jquery.gsap` plugings configure themselves, loading them here, in this order, makes them available to TweenLite.

Create other modules to load and access more plugins, easings, or the more powerful TweenMax / TimelineMax.
Any/all of the [GSAP](http://greensock.com/gsap) modules can be used.

Examples: [`app/modules/greensock-examples`](./js-modules.html#app/modules/greensock-examples)


*/
/* jshint unused:false */
(function() { "use strict";
    define([
		"TweenLite",
		"libs/greensock/plugins/CSSPlugin",
		"libs/greensock/jquery.gsap"
	], function (TweenLite, CSSPlugin, jQueryPlugin) {
        return TweenLite;
    });
}());