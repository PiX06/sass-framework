/*doc
---
title: Logger
name: core/logger
category: JS - core 
---
[`core/logger`](../../assets/js/core/logger.js)

Logger utility 
`log, info, warn, error, enable, disable, setPrefix`

*/
/*globals define*/
(function (window) { "use strict";
    define([], function () {

        /*jshint newcap: true */
        if (!Function.prototype.bind) {
            Function.prototype.bind = function(oThis) {
                /*jshint newcap: false */
                if (typeof this !== 'function') {
                    // closest thing possible to the ECMAScript 5
                    // internal IsCallable function
                    throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
                }

                var aArgs   = Array.prototype.slice.call(arguments, 1),
                    fToBind = this,
                    fNOP    = function() {},
                    fBound  = function() {
                        return fToBind.apply(this instanceof fNOP && oThis ? this : oThis,
                            aArgs.concat(Array.prototype.slice.call(arguments)));
                    };

                fNOP.prototype = this.prototype;
                fBound.prototype = new fNOP();

                return fBound;
            };
        }

        var
            L     = null,
            enabled=false,
            prefix = null,

            isNative = function(fn) {
                // IE11  doesn't have `[native code]` in the toString of console.log, but it does have 'safeAssert' which seems like a reasonable thing to test...
               return /^\s*function[^{]+{\s*\[native code\]\s*}\s*$|safeAssert/.test(fn);
            },

            setup = function(){

                if(typeof window.console !== "undefined" && typeof window.console.log !== 'undefined' && isNative(window.console.log) === true){
                    L = window.console;
                    // fix ie8/9 - bind/apply

                    if (typeof Function.prototype.bind !== 'undefined' && typeof console.log === "object"){
                        if (typeof Array.prototype.forEach === "undefined") {  // IE8 Array forEach
                            Array.prototype.forEach = function (fn, scope) {
                                var i, len = this.length;
                                for (i = 0; i < len; ++i) {
                                    fn.call(scope || this, this[i], i, this);
                                }
                            };
                        }
                        ["log","info","warn","error","assert","dir","clear","profile","profileEnd"].forEach(function (method) {
                            console[method] = this.bind(console[method], console);
                        }, Function.prototype.call);
                    }

                } else {
                    L = {fake:true, log:function(){},info:function(){},warn:function(){},error:function(){}};
                    window.console = L;
                }

                enabled = true;

                // of course, ie8 doesn't have Object.freeze
                try{ Object.freeze(L); } catch(err){}
            },


            applyPrefix = function(args){
                if(prefix !== null) {
                    if(typeof args[0] === "string"){
                        args[0] = prefix + args[0];
                    } else {
                        var out = [], outSymbols = [], i, n = args.length;
                        for(i=0;i<n;i++){
                            switch(typeof args[i]){
                               case "object": outSymbols.push("%o");
                                    break;
                                default: outSymbols.push(" %s");
                                    break;
                            }
                            out.push(args[i]);
                        }

                        out.unshift(prefix + outSymbols.join(""));
                        return out;
                    }
                }
                return args;
            },

            log = function(){  if(enabled) { L.log.apply(window.console, applyPrefix(arguments)); } },
            info = function(){ if(enabled) { L.info.apply(window.console, applyPrefix(arguments)); } },
            warn = function(){  if(enabled) { L.warn.apply(window.console, applyPrefix(arguments)); } },
            error = function(){ if(enabled) { L.error.apply(window.console, applyPrefix(arguments)); } },
            setEnabled = function(value){ enabled = value; };

            setup();

        return {

            log:log,
            info:info,
            warn:warn,
            error:error,

            disable:function(){
                setEnabled(false);
            },

            enable:function(){
                setEnabled(true);
            },

            setPrefix : function(p){
                prefix = ((typeof p === 'undefined' || p === "") ? null : p);
            }
        };
    });
}(window));
