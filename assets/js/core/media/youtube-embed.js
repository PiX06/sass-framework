/*doc
---
title: YouTube Embed
name: core/media/youtube-embed
category: JS - core ~ media
---
[`core/media/youtube-embed`](../../assets/js/core/media/youtube-embed.js)

Utility that inserts a YouTube iframe player embed for a given video-id, and provides access to the JS API for the player.

In the example below, the container DIV  with the `data-video-type="youtube"` atttribute will be replaced by a YouTube player iframe.
Attributes set on the div will be carried accross to the generated iframe. 

The `id` of the video container is used to target each player. If you don't specify one in the mark-up one will be generated automatically.

Generated IDs will be in the format of `ytiframe_[video-id]` OR `ytiframe_[video-id]_[random-alphanumeric-string]` if there is more than one player with the same video-id...

```html_example
    <div class="video-player">
        <div class="responsive-media-container">   
            <div data-video-type="youtube" data-video-id="uaGEjrADGPA" class="responsive-media"></div>
        </div>
    </div>
    
    <br/><p>Another one? Why not! Multiple embeds are supported.</p>
    
    <div class="video-player">
        <div class="responsive-media-container">   
            <div data-video-type="youtube" data-video-id="FODvjYoVEi8" class="responsive-media" ></div>
        </div>
    </div>    
```

##### Initialise...
`init()`

Start loading the YouTube JS API scripts if not already available`

##### Setup...
`start()`

Pass nothing, or a custom selector (css/jquery) to specify elements that should be processed as video containers.

The `data-video-id` attribute is required on the container, and should be the id of the video you want to embed.

The default selector is `div[data-video-type="youtube"]` - you can override that by passing any css/jquery selector compatible string as the the 1st parameter to target one or more elements of your choice.

`start('.my-custom-selector-for-youtube-contanier-or-containers');`

##### functions
`getAPI()` `getPlayer(id)` `getPlayerIsReady(id)`

##### signals
`apiReady` `playerReady` `playerStateChanged`

*/

/* jshint bitwise:false */
(function(w, d){ "use strict";
	define([ "jquery", "signals", "core/logger", "core/util/randomString"],
	function ($, signals, Logger, randomString) {
        var             
            idPrefix = "ytiframe_",
            
            YT = null, // the youtube api - a reference to the window.YT object
            loadingAPI = false,
            
            players = {},
            playersReady = {},
            
            apiReady = new signals.Signal(),
            playerReady = new signals.Signal(),
            playerStateChange = new signals.Signal(),
            
            /**
             * Generate a unique dom id for a video player. All players are accisible/identified by their id.
             * @param videoID
             * @returns {string}
             */
            generateContainerID = function(videoID){
                var id = idPrefix + videoID;   
                while(d.getElementById(id) !== null){ // if the id already exists... find a new one.
                    id = idPrefix + videoID + '_' + randomString();
                }
                return id;
            },
            
            
            onAPIReady = function(){
                Logger.log('[YouTubeEmbed] onAPIReady');
                loadingAPI = false;
                YT = w.YT;
                w.onYouTubeIframeAPIReady = null;
                apiReady.dispatch(YT);
            },
            
            
            loadAPI = function(){
                
                Logger.log('[YouTubeEmbed] loadAPI');
                
                loadingAPI = true;
                w.onYouTubeIframeAPIReady = onAPIReady;
                
                var tag = d.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = d.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);                    
            },
            
            
            haveAPI = function(){
                return (loadingAPI === false) && (typeof w.YT !== 'undefined') && (typeof w.YT.Player === "function");
            },
            
            
            getAPI = function(){ return YT; },
            
            
            getPlayerIsReady = function(id){
                return playersReady[id] === 'undefined' ? false : playersReady[id];
            },
            
            
            getPlayer = function(id){
                return players[id] === 'undefined' ? null : players[id];
            },
            
            
            onPlayerReady = function(e) {         
                Logger.log('[YouTubeEmbed] onPlayerReady - %s', e.target.ffid); 
                playersReady[e.target.ffid] = true;
                playerReady.dispatch(e.target);
            },


            /**
             * Player State Change handler - triggers the playerStateChange signal, which emits both the Player object and State number
             * 
             * You can also call player.getPlayerState() to directly check the current state, check the YouTube JS API docs for more.
             * 
             * @param e
             */
            onPlayerStateChange = function(e) {
                Logger.log('[YouTubeEmbed] onPlayerStateChange player:%s, state:%s', e.target.ffid, e.data);
                playerStateChange.dispatch(e.target, e.data);                
                /* NOTE: - use the YT.PlayerState enum to see what the state is...
                switch(state){
                    case YT.PlayerState.BUFFERING: Logger.log('BUFFERING'); break;                    
                    case YT.PlayerState.CUED: Logger.log('CUED'); break;                    
                    case YT.PlayerState.ENDED: Logger.log('ENDED'); break;                    
                    case YT.PlayerState.PAUSED: Logger.log('PAUSED'); break;
                    case YT.PlayerState.PLAYING: Logger.log('PLAYING'); break;    
                    case YT.PlayerState.UNSTARTED: Logger.log('UNSTARTED'); break;
                    default: throw 'Unexpected player state: ' + e.data; break;
                }
               */
            },

            
            /**
             * 
             * @param containerId
             * @param videoId
             * @returns {boolean} true if a player was successfully created, false if not.
             */
            createPlayer = function(containerId, videoId){         
               
                if(YT === null) {
                    Logger.warn('[YouTubeEmbed] createPlayer - The YouTube API has not loaded yet. Wait for the apiReady signal first...');
                    return false;
                }
                
                if(typeof players[containerId] !== 'undefined') {
                    Logger.warn('[YouTubeEmbed] createPlayer - A player with the id "%s" has already been set up.', containerId);
                    return false;
                }
                
                // create it
                players[containerId] = new YT.Player(containerId, {
                    videoId: videoId,
                    width:'100%', height:'100%',
                    playerVars : { modestbranding: 1, showinfo:0, fs:1, wmode:'transparent' },                   
                    events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange }
                });
                
                // store the container id on the player instance
                players[containerId].ffid = containerId;
                
                return true;
            },
            
            
            init = function() {
                Logger.log('[YouTubeEmbed] init');                
                if(haveAPI() === false){ loadAPI(); }
            },
            
            
            /**
             * Set-up youtube player(s) for a given selector
             * @param containerSelector - optional, defaults to 'div[data-video-type="youtube"]'#
             *
             * @returns $containers - jQuery object of the DOM elements matching the container selector.
             */
            start = function(containerSelector){
                
                var selector = (typeof containerSelector === 'undefined') ? 'div[data-video-type="youtube"]' : containerSelector,
                    $containers = $(selector),
                    n = $containers.length;
                
                if(n > 0){
                    
                    Logger.log('[YouTubeEmbed] start - setting up a youtube iframe for %s element(s) matching the selector "%s"', n, selector);
                
                    var $container;
                    
                    $containers.each(function() {
                        
                        $container = $(this);
                        
                        if($container.is('iframe') === true){
                            Logger.warn("[YouTubeEmbed] start - the selected container is already an iframe... has it been setup already?");
                        }

                        var videoID = $container.attr('data-video-id'),
                            containerId = $container.attr('id'); 
                        
                        // if the video-container already has an id, use that, otherwise generate one for it.
                        if (typeof containerId === 'undefined' || containerId.length === 0) {
                            containerId = generateContainerID(videoID);
                            $container.attr('id', containerId);
                        }
                        
                        if (typeof videoID === 'undefined' || videoID.length === 0) {
                            
                            Logger.warn("[YouTubeEmbed] start - embed container is missing the 'data-video-id' attribute - unable to initialise.");
                            
                        } else {
                            
                            Logger.log('[YouTubeEmbed] start - setting up for element %s with videoID %s', containerId, videoID);
                            
                            if(haveAPI() === true){
                                createPlayer(containerId, videoID);
                            } else {
                                 apiReady.addOnce(function () { createPlayer(containerId, videoID); });
                            }
                        }
                    });
                }
                
                return $containers;
            };
        
		return { 
            //
			init                : init,
			start               : start,
            
            // Signals
            apiReady            : apiReady, /* Signal<Void->Void> */
            playerReady         : playerReady, /* Signal<Player->Void> */
            playerStateChange   : playerStateChange, /* Signal<Player->State<Int>->Void> */ /* @see YT.PlayerState */ 
            
            // 
            getAPI              : getAPI, /* YT */
            getPlayer           : getPlayer, /* by id */
            getPlayerIsReady    : getPlayerIsReady /* by id */
		};
	});
})(window, document);