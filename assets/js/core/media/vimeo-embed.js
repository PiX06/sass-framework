/*doc
---
title: Vimeo Embed
name: core/media/vimeo-embed
category: JS - core ~ media
---
[`core/media/vimeo-embed`](../../assets/js/core/media/vimeo-embed.js)


```html_example    
    <div class="video-player">
        <div class="responsive-media-container">   
            <div data-video-type="vimeo" data-video-id="10785201" class="responsive-media"></div>
        </div>
    </div>    
    
    <br/>
    <p>Another?</p>
    
    <div class="video-player">
        <div class="responsive-media-container">   
            <div data-video-type="vimeo" data-video-id="31352350" class="responsive-media"></div>
        </div>
    </div> 
```

Note: Vimeo Player instance functions and properties are documented here... 
[https://github.com/gusnips/vimeo-js-api](https://github.com/gusnips/vimeo-js-api)

TODO: do these vimeo player+api docs

*/

/*

Vimeo player properties...
=-=-=-=-=-=-=-=-=-=-=-=-=-

.element
.init .api .load .addEvent .removeEvent
.play .pause .unload .seek .stop .seekTo .paused
.setVolume .setLoop .setColor 
.getVolume .getLoop .getColor
.getCurrentTime .getDuration
.getVideoWidth .getVideoHeight
.getVideoUrl .getVideoEmbedCode

.onReady .offReady .onPlay .offPlay
.onPause .offPause .onSeek .offSeek
.onFinish .offFinish .onPlayProgress .offPlayProgress
.onLoadProgress .offLoadProgress
*/

/* jshint bitwise:false */
(function (d) { "use strict";    
    
    define(["jquery", "signals", "core/logger", "core/util/randomString", "libs/vimeo-api"],

        function ($, signals, Logger, randomString, Vimeo) {
            
            var
                idPrefix = "vimIframe_",
                iframeTemplate = '<iframe id="::playerId::" src="//player.vimeo.com/video/::videoId::?player_id=::playerId::&api=1" ::moreAttributes:: width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',

                players = {},
                playersReady = {},
                playerReady = new signals.Signal(),
                playerEvent = new signals.Signal(),

                getPlayerIsReady = function(id){
                    return playersReady[id] === 'undefined' ? false : playersReady[id];
                },

                getPlayer = function(id){
                    return players[id] === 'undefined' ? null : players[id];
                },

                /**
                 * Generate a unique dom id for a video player. All players are accisible/identified by their id.
                 * @param videoID
                 * @returns {string}
                 */
                generateContainerID = function(videoID){
                    var id = idPrefix + videoID;
                    while(d.getElementById(id) !== null){ // if the id already exists... find a new one.
                        id = idPrefix + videoID + '_' + randomString();
                    }
                    return id;
                },

                
                setupPlayerEventSignal = function(id){  
                    var d = playerEvent.dispatch;
                    players[id]
                        .onPlay(function() { d('play', id); })
                        .onPause(function() { d('pause', id); })
                        .onFinish(function() { d('finish', id); })
                        .onSeek(function(data) { d('seek', id, data); })
                        .onLoadProgress(function(data) { d('loadProgress', id, data); })
                        .onPlayProgress(function(data) { d('playProgress', id, data); });                        
                },
                
                
                onPlayerReady = function(id){
                    if(playersReady[id] !== true){         
                        players[id].offReady();
                        setupPlayerEventSignal(id);                       
                        playersReady[id] = true;
                        playerReady.dispatch(players[id]);       
                    }
                },
                

                start = function(containerSelector){

                    Logger.log('[VimeoEmbed] init');

                    var selector = (typeof containerSelector === 'undefined') ? 'div[data-video-type="vimeo"]' : containerSelector,
                        $containers = $(selector);

                    if($containers.length > 0){ Logger.log('[VimeoEmbed] setting up %s iframe player(s) matching the selector "div[data-video-type="youtube"]"', $containers.length); }

                    $containers.each(function(){

                        var $this = $(this),
                            attrs = this.attributes,
                            videoId = $this.attr('data-video-id'),
                            playerId = $this.attr('id');

                        if(typeof videoId !== 'undefined' && videoId.length > 0) {

                            if(typeof playerId === 'undefined' || playerId.length === 0){ playerId = generateContainerID(videoId); }

                            Logger.log('[VimeoEmbed] init - setting up player %s with videoID %s', playerId, videoId);
                            
                            // copy any/all attributes to the iframe...
                            var i, n = attrs.length, extraAttributes = '';
                            for (i = 0; i < n; i++) {
                                extraAttributes += attrs[i].name + '="' + attrs[i].value + '" ';
                            }

                            var iframeMarkup = iframeTemplate
                                .replace(/::videoId::/, videoId)
                                .replace(/::playerId::/g, playerId)
                                .replace(/::moreAttributes::/, extraAttributes);

                            $this.replaceWith(iframeMarkup);
                            
                            players[playerId] = new Vimeo(playerId);
                            playersReady[playerId] = false;
                            players[playerId].onReady(onPlayerReady);
                            
                        } else {
                            Logger.warn("[VimeoEmbed] start - embed container is missing the 'data-video-id' attribute - unable to initialise.");
                            Logger.warn($this);
                        }
                    });
                },

                init = function(){
                    Logger.warn("[VimeoEmbed] init - nothing to init.");
                };
            
            return {
                
                init                : init,
                start               : start,
                //
                playerReady         : playerReady, /* Signal<Player->Void> */
                playerEvent         : playerEvent, /* Signal<String->String->Dynamic->Void> */ /* @see event names/types below */ 
                // 
                getPlayer           : getPlayer, /* by id */
                getPlayerIsReady    : getPlayerIsReady, /* by id */
                
                PLAY:'play',
                PAUSE:'pause',
                FINISH:'finish',   
                // these ones also send a data object..
                SEEK:'seek',
                LOAD_PROGRESS:'loadProgress',
                PLAY_PROGRESS:'playProgress'                
            };    
    });
})(document);
