/*doc
---
title: Accordion
name: core/accordion
category: JS - core
---
[`core/accordion`](../../assets/js/core/accordion.js)

A responsive, mobile first, accordion/tabs module.  
Uses [`core/breakpoints`](#core/breakpoints) to trigger a rebuild if/when the layout changes.

Page mark-up should always be for an accordion - mobile first.

Tab layouts are created programatically; configure for specific breakpoints using the `data-tabs-enabled` attribute. Typical use would be for desktop or wide layouts.


```html_example

<div class="js-accordion-container accordion-container" data-tabs-enabled="desk" data-tab-class="testing-tab-container">
	<div class="js-accordion-item js-accordion-open">
		<button type="button" role="tab" class="js-accordion-button accordion-button" data-tab-class="testing-tab-header1">Accordion button 1</button>
		<div class="js-accordion-content accordion-content" role="tabpanel" data-tab-class="testing-tab-item1">
			<p>HELLO WORLD</p>
			<p><a href="http://google.co.uk" target="_blank">GOODBYE WORLD</a></p>
		</div>
	</div>
	<div class="js-accordion-item">
		<button type="button" role="tab" class="js-accordion-button accordion-button" data-tab-class="testing-header2">Accordion button 2</button>
		<div class="js-accordion-content accordion-content" role="tabpanel" data-tab-class="testing-tab-item2">
			<p>HELLO WORLD 2</p>
		</div>
	</div>
</div>

<br/><hr/>
<code>data-tabs-enabled="mob" data-tabs-nav="true"</code>
<div class="js-accordion-container accordion-container" data-tabs-enabled="mob" data-tabs-nav="true">
	<div class="js-accordion-item accordion-item">
		<button type="button" role="tab" class="js-accordion-button accordion-button">I am a tab on mob</button>
		<div class="js-accordion-content accordion-content" role="tabpanel">
			<p>Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
		</div>
	</div>
	<div class="js-accordion-item accordion-item">
		<button type="button" role="tab" class="js-accordion-button accordion-button">I am also tab on mob</button>
		<div class="js-accordion-content accordion-content" role="tabpanel">
			<p>Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Phasellus ipsum. Nunc tristique tempus lectus.</p>
		</div>
	</div>
</div>

<br/><hr/>
<code>data-accordion-disabled="desk"</code>
<div class="js-accordion-container accordion-container" data-accordion-disabled="desk">
	<div class="js-accordion-item accordion-item">
		<button type="button" role="tab" class="js-accordion-button accordion-button">Accordion button 1</button>
		<div class="js-accordion-content accordion-content" role="tabpanel">
			<p>HELLO WORLD 1</p>
		</div>
	</div>
	<div class="js-accordion-item accordion-item">
		<button type="button" role="tab" class="js-accordion-button accordion-button">Accordion button 2</button>
		<div class="js-accordion-content accordion-content" role="tabpanel">
			<p>HELLO WORLD 2</p>
		</div>
	</div>
	<div class="js-accordion-item accordion-item">
		<button type="button" role="tab" class="js-accordion-button accordion-button">Accordion button 3</button>
		<div class="js-accordion-content accordion-content" role="tabpanel">
			<p>HELLO WORLD 3</p>
		</div>
	</div>
</div>

<br/><hr/>

<code>data-close-sibling-accordions="true"</code>
<div class="js-accordion-container accordion-container" data-close-sibling-accordions="true">
    <div class="js-accordion-item accordion-item">
        <button type="button" role="tab" class="js-accordion-button accordion-button">Accordion button 1</button>
        <div class="js-accordion-content accordion-content" role="tabpanel">
            <p>HELLO WORLD 1</p>
        </div>
    </div>
    <div class="js-accordion-item accordion-item">
        <button type="button" role="tab" class="js-accordion-button accordion-button">Accordion button 2</button>
        <div class="js-accordion-content accordion-content" role="tabpanel">
            <p>HELLO WORLD 2</p>
        </div>
    </div>
    <div class="js-accordion-item accordion-item">
        <button type="button" role="tab" class="js-accordion-button accordion-button">Accordion button 3</button>
        <div class="js-accordion-content accordion-content" role="tabpanel">
            <p>HELLO WORLD 3</p>
        </div>
    </div>
</div>

<br/><hr/>

<code>data-tabs-enabled="desk" data-tabs-nav="true"</code>
<div class="js-accordion-container accordion-container" data-tabs-enabled="desk" data-tabs-nav="true" data-keep-classes="">
	<div class="js-accordion-item accordion-item">
		<button type="button" role="tab" class="js-accordion-button accordion-button">Nunc tincidun</button>
		<div class="js-accordion-content accordion-content" role="tabpanel">
			<p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
		</div>
	</div>
	<div class="js-accordion-item accordion-item">
		<button type="button" role="tab" class="js-accordion-button accordion-button">Proin dolor</button>
		<div class="js-accordion-content accordion-content" role="tabpanel">
			<p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
			<div>I am a test</div>
            <a href="http://google.co.uk" target="_blank">GOODBYE WORLD</a>
		</div>
	</div>
	<div class="js-accordion-item accordion-item js-accordion-open">
		<button type="button" role="tab" class="js-accordion-button accordion-button">Aenean lacinia</button>
		<div class="js-accordion-content accordion-content" role="tabpanel">
			<div>
                <p>HELLO WORLD</p>
                <p><a href="http://google.co.uk" target="_blank">GOODBYE WORLD</a></p>
                <ul>
                    <li><a href="#" >test a</a></li>
                    <li><a href="#" >test b</a></li>
                    <li><a href="#" >test c</a></li>
                </ul>
			</div>
			<p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
			<p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
		</div>
	</div>
</div>
```

*/

(function () { "use strict";
    define(['jquery', 'signals', 'core/logger', 'core/breakpoints', 'core/tween'], function ($, signals, Logger, Breakpoints/*, Tween*/) {
        
        var
			$containers,
            beforeBuild = new signals.Signal(),
            afterBuild = new signals.Signal(),
            
            activeTabChange = new signals.Signal(),
            
            jsContainerClass = 'js-accordion-container',
            jsButtonClass   = '.js-accordion-button',
            jsContentClass  = '.js-accordion-content',
            
            jsOpenClass     = "js-accordion-open",
            jsItemClass     = "js-accordion-item",
            
            jsTabsContainer = 'js-tabs-container',
            jsTabsHeaderNav = 'js-tabs-nav',
            jsTabHeader     = 'js-tab-header',
            jsTabHeaders    = 'ul > li.' + jsTabHeader,
			jsActiveTab	    = 'active-tab', // for tabs mode - set on a tab header <li>
			jsTabContent    = 'js-tabs-content', 
            
			// build tabs from the in-page accordion markup and the following templates...
			tabsContainerTemplate   = '<div class="'+jsTabsContainer+' ::extraClasses::">::inner::</div>',
			tabsHeadersTemplate     = '<ul class="'+jsTabsHeaderNav+' tabs-nav nav" role="tablist">::inner::</ul>',
			tabsHeaderItemTemplate  = '<li class="'+jsTabHeader+' ::extraClasses::" aria-controls="item-::controls::"><a href="#item-::index::">::label::</a></li>',
			tabsItemContentTemplate = '<div id="item-::id::" class="'+jsTabContent+' ::extraClasses::" style="display:none;" role="tabpanel">::content::</div>',
            
            tabsNavPrev = 'js-prev',
            tabsNavNext = 'js-next',
            tabsNavPrevNext = 'js-tabs-nav-prev-next',
            tabsNavPrevNextTemplate = 
                '<div class="' + tabsNavPrevNext + '">' +
                    '<button class="' + tabsNavPrev + ' disabled">prev</button>' +
                    '<button class="' + tabsNavNext + '">next</button>' +
                '</div>',
            
            
            /* Tabs */
            /* ------------------------------------------------------------------------------------------------------ */
            
            /**
             * 
             * @param $container
             * @returns {*|jQuery|HTMLElement}
             */
			generateTabsMarkup = function($container, hasTabsNav){
				// build tabs markup from existing accordion DOM
				 var $accordionButton, $accordionContent, 
                     tabsHeaderItems = '', tabsItemContents = '', tabClasses,
                     $item, $tabsContainer, $items = $('.' + jsItemClass, $container);                  
                
				$items.each(function(i,o) {
					$item = $(o);
                    $accordionButton = $(jsButtonClass, $item);
                    $accordionContent = $(jsContentClass, $item);
                    
                    // header
                    tabClasses = $accordionButton.attr('data-tab-class');
                    if(typeof tabClasses === 'undefined') { tabClasses = ''; }
                    //
					tabsHeaderItems +=
						tabsHeaderItemTemplate
                            .replace('::controls::', i)
							.replace('::index::', i)
							.replace('::extraClasses::', tabClasses)
							.replace('::label::', $accordionButton.text());

                    // content
                    tabClasses = $accordionContent.attr('data-tab-class');
                    if(typeof tabClasses === 'undefined') { tabClasses = ''; }
                    //
					tabsItemContents +=
						tabsItemContentTemplate
                            .replace('::id::', i)
							.replace('::extraClasses::', tabClasses)
							.replace('::content::', $accordionContent.html());
				});
                
                // container
                tabClasses = $container.attr('data-tab-class');
                if(typeof tabClasses === 'undefined') { tabClasses = ''; }
                //
                $tabsContainer = 
                    $(tabsContainerTemplate
                        .replace('::extraClasses::', tabClasses)
                        .replace('::inner::', tabsHeadersTemplate.replace('::inner::', tabsHeaderItems) + tabsItemContents)
                    );
                
                if(hasTabsNav){ // bottom next/prev nav
                    $tabsContainer.data('tabs-nav', true);
                    $tabsContainer.append(tabsNavPrevNextTemplate);
                }
                
                return $tabsContainer;
			},

            /**
             * 
             * @param $tabsContainer
             * @returns {int}
             */
            getSelectedTabIndex = function($tabsContainer) {
                var i = parseInt($tabsContainer.data('selectedIndex'), 10);
                return isNaN(i) ? -1 : i;
            },

            /**
             * 
             * @param $tabsContainer
             * @returns {*}
             */
            tabItemsCount = function($tabsContainer) {
                return parseInt($tabsContainer.data('itemCount'), 10);
            },
            
            /**
             * @param $tabsContainer
             * @param direction 1/-1 (next/prev)
             */
           selectNextPrevTab = function($tabsContainer, direction){
                
               direction = parseInt(direction, 10);
               if(isNaN(direction)){ direction = 1; }
                
                var n = tabItemsCount($tabsContainer),
                    i = getSelectedTabIndex($tabsContainer),
                    targetIndex = i + direction;
                
                if(!(targetIndex === i || targetIndex < 0 || targetIndex >= n ) ) {
                    setActiveTab($tabsContainer, targetIndex);
                }                
            },

            /**
             * 
             * @param $tabsContainer
             * @param index
             */
			setActiveTab = function($tabsContainer, index){
                
                if (index < 0) { index = 0; }
                
				var $tabsHeaders = $(jsTabHeaders, $tabsContainer),
					$tabsContents = $('div.' + jsTabContent, $tabsContainer);
                
                // store selected index
                $tabsContainer.data('selectedIndex', index);
                
				// remove old
				$tabsHeaders.removeClass(jsActiveTab).attr('aria-selected', 'false');
				$tabsContents.css({display:'none'}).attr('aria-hidden', 'true');

				// set new
				$($tabsHeaders.get(index)).addClass(jsActiveTab).attr('aria-selected', 'true');
				$($tabsContents.get(index)).css({display:''}).attr('aria-hidden', 'false');

                
                if($tabsContainer.data('tabs-nav') === true){ 
                    // has prev/next tab nav
                    var n = tabItemsCount($tabsContainer);
                    if(index === 0){
                        $tabsContainer.find('.js-prev').addClass('disabled');
                        if(n > 1) { $tabsContainer.find('.js-next').removeClass('disabled'); }
                    } else if(index === n-1){
                        $tabsContainer.find('.js-next').addClass('disabled');
                        $tabsContainer.find('.js-prev').removeClass('disabled');
                    } else {
                        $tabsContainer.find('.js-prev, .js-next').removeClass('disabled');
                    }
                }
                
                activeTabChange.dispatch($tabsContainer, index);
			},
            

            /**
             * 
             * @param $container
             */
			setupTabs = function($container){

				$container.css({display:'none'});

				var $tabsContainer = $container.next('.js-tabs-container'),
                    hasTabsNav = $container.data('tabs-nav') === true,
					$tabsContents, $tabsHeaders;

				if( $tabsContainer.length === 0 ) { // have not yet setup tabs for this item
                    
					$tabsContainer = generateTabsMarkup($container, hasTabsNav);
					$tabsHeaders = $(jsTabHeaders, $tabsContainer);
					$tabsContents = $('div.' + jsTabContent, $tabsContainer);
                    
                    $tabsContainer.data('itemCount', $tabsHeaders.length);
                    
					$tabsHeaders.each(function(i, o){
						$('a', o).on('click tap', function(e){
							e.preventDefault();
							var $items = $('.' + jsItemClass, $container);
							// remove jsOpenClass from any active accordion panel(s)
							$items.removeClass(jsOpenClass);
							// activate the selected tab
							setActiveTab($tabsContainer, i);
							// add jsOpenClass to the appropriate accordion panel so it will open if/when layout changes back to accordion.
							$($items.get(i)).addClass(jsOpenClass);
							//
							return false;
						});
					});
                    
                    if(hasTabsNav) {
                        $('.' + tabsNavPrevNext + ' > .' + tabsNavPrev + ', .' + tabsNavNext, $tabsContainer)
                            .on('click tap', function (e) {
                                e.preventDefault();
                                var $this = $(this);
                                selectNextPrevTab($this.parents().closest('.js-tabs-container'), ($this.hasClass('js-next') ? 1 : -1));
                                return false;
                            });
                    }
                    
					$tabsContainer.insertAfter($container);

				} else { // have generated tabs markup already, so hide the contents, and reveal the container

					$tabsHeaders = $(jsTabHeaders, $tabsContainer);
					$tabsHeaders.removeClass(jsActiveTab);
					
                    $tabsContents = $('div.' + jsTabContent, $tabsContainer);
					$tabsContents.css({display:'none'});
                    
					$tabsContainer.css({display:''});

				}
                
				// get index of 1st open accordion item...  and select the corresponding tab
				setActiveTab($tabsContainer, $('.' + jsOpenClass, $container).first().index());
			},
            

            
            /* Accordion */
            /* ------------------------------------------------------------------------------------------------------ */
            
            /**
             * 
             * @param e
             * @returns {boolean}
             */
			onAccordionButtonClick = function(e){
				e.preventDefault();
				e.data.content.not(":animated").slideToggle(250, function() {
                    e.data.item.toggleClass(jsOpenClass);

                    setAriaAttributes(e.data.item, e.data.button, e.data.content);
                });

				return false;
			},

            setAriaAttributes = function($item, $button, $content) {
                if($item.hasClass(jsOpenClass)) {
                    $button.attr('aria-expanded', 'true');
                    $content.attr('aria-hidden', 'false');
                } else {
                    $button.attr('aria-expanded', 'false');
                    $content.attr('aria-hidden', 'true');
                }
            },

            closeSiblingAccordions = function(e) {

                var accordionSiblings = e.data.item.siblings();

                accordionSiblings.find(jsContentClass).slideUp(250);
                
                accordionSiblings.removeClass(jsOpenClass);
                accordionSiblings.find(jsButtonClass).attr('aria-expanded', 'false');
                accordionSiblings.find(jsContentClass).attr('aria-hidden', 'true');

            },

            /**
             * 
             * @param $container
             */
            setupAccordion = function($container){
                // next sibling will be js-tabs-container if tabs are enabled for a breakpoint on this accordion
                var $tabs = $container.next('.js-tabs-container'),
                    closeSiblingsOnClick = $container.data('close-sibling-accordions') === true;

                if ($tabs.length !== 0) {
                    $tabs.css({display: 'none'});
                    $container.css({display: ''});
                }

                var $button, $content, $item, $items = $('.' + jsItemClass, $container);

                $container.attr({'role': 'tablist', 'aria-multiselectable': 'true'});

                $items.each(function () {
                    $item = $(this);
                    $button = $(jsButtonClass, $item);
                    $content = $(jsContentClass, $item);

                    if ($item.hasClass(jsOpenClass)) {
                        $content.css({display: ''});
                    } else {
                        $content.css({display: 'none'});
                    }

                    setAriaAttributes($item, $button, $content);
                    
                    $button.on('click tap', null, {item: $item, content: $content, button: $button}, onAccordionButtonClick);

                    if(closeSiblingsOnClick) {
                        $button.on('click tap', null, {item: $item, content: $content, button: $button}, closeSiblingAccordions);
                    }

                });

                $container.data('built', true);
            },

            
            
            /* Build / Setup */
            /* ------------------------------------------------------------------------------------------------------ */
            
            /**
             * 
             * @param $container
             * @param tabsMode
             */
            build = function ($container, tabsMode) {

                beforeBuild.dispatch(tabsMode);
                
				if($container.data('built') === true) { 
                    remove($container);
                }

				if(tabsMode){
					setupTabs($container);
				} else {                    
                    setupAccordion($container);
				}
                
                afterBuild.dispatch(tabsMode);
            },

            /**
             * 
             * @param $container
             */
            remove = function ($container) {
               if($container.data('built') === false) { return; }

			   var $button, $content,
					$item, $items = $('.' + jsItemClass, $container);

				$items.each(function(){
					$item = $(this);
					$button = $(jsButtonClass, $item);
					$content= $(jsContentClass, $item);

                    $button.off('click tap');
                    $content.css({display:''});
                });

				$container.data('built', false);
            },

            /**
             * 
             * @param current
             */
            onBreakpointChange = function(current){

				var $container, data, doBuild;
				$containers.each(function(){
					$container = $(this);
					doBuild = true;

					data = $container.data('accordion-disabled');
					if(typeof data !== 'undefined'){ // have disabled data
						if(data.indexOf(current) !== -1){ // current is disabled?
							doBuild = false;
						}
					} else {
						data = $container.data('accordion-enabled');
						if(typeof data !== 'undefined'){ // have enabled data
							if(data.indexOf(current) === -1){ // current is not enabled?
								doBuild = false;
							}
						}
					}

					var tabsMode = false;
					data = $container.data('tabs-enabled');
					if(typeof data !== 'undefined' && data.indexOf(current) !== -1){
						if(!doBuild){
							Logger.warn("Misconfigured accordion/tab stuff?");
							doBuild = true;
						}
						tabsMode = true;
					}

					if(doBuild === true){
						build($container, tabsMode);
					} else {
						remove($container);
					}
				});
            },

            
            
            /* Public */
            /* ------------------------------------------------------------------------------------------------------ */
            
			/**
			 * Open all accordion panels
			 * @param $container optional - set a specific accordion container (will affect all accordions if not set)
			 */
			openAll = function($container){
				if(typeof $container === 'undefined'){ $container = $containers; }
				$('.js-accordion-item', $container)
					.not('.' + jsOpenClass)
					.find(jsButtonClass)
					.trigger('click');
			},

			/**
			 * Close all accordion panels
			 * @param $container optional - set a specific accordion container (will affect all accordions if not set)
			 */
			closeAll = function($container){
				if(typeof $container === 'undefined'){ $container = $containers; }
				$('.' + jsOpenClass + ' > ' + jsButtonClass, $container).trigger('click');
			},

			/**
			 * Toggle the state of all accordion panels
			 * @param $container optional - set a specific accordion container (will affect all accordions if not set)
			 */
			toggleAll = function($container){
				if(typeof $container === 'undefined'){ $container = $containers; }
				$(jsButtonClass, $container).trigger('click');
			},

			/**
			 * Stop wathcing for breakpoint/layout changes
			 * @param andRemove optional - set to also disable the accordion behavoiours
			 */
            stop = function (andRemove) {
               if($containers.length > 0){
					Logger.log("[core/accordion] stop");
					if(andRemove === true) {
						$containers.each(function(){ remove($(this)); });
					}
					Breakpoints.layoutChange.remove(onBreakpointChange);
				}
			},
            
			/**
			 * Start wathcing for breakpoint/layout changes
			 */
            start = function () {

				// pick valid accordions - containers with child items...
				$containers = $('.' + jsContainerClass).filter( function() { return $(this).children().hasClass(jsItemClass); } );

				if($containers.length > 0){
					Logger.log("[core/accordion] start - found %s containers", $containers.length);
					$containers.data('built', false);
					Breakpoints.layoutChange.add(onBreakpointChange);
				}
            };

        return {
			start       : start,
			stop        : stop,
            
            /* These behaviours only apply to accordions - not tabs */
			openAll     : openAll,
			closeAll    : closeAll,
			toggleAll   : toggleAll,
            
            /* Signals that fire just before, and just after, the layout changes to/from accordion/tabs */
			beforeBuild : beforeBuild, 
			afterBuild  : afterBuild,
            
            // Signal that fires when the active tab changes (only applies to tabs mode) 
            // Supplies 2 parameters to listeners - a jQuery object of the tabs-container and the index of the selected tab
            activeTabChange : activeTabChange /* Signal<jQuery->Int->Void> */
		};
    });
}());
