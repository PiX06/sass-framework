/*doc
---
title: randomString
name: core/util/randomString
category: JS - core - util
---

Genreate a random int between 0 and 0xfffffff and output as a Base36 String.

```js-example
(function(){
    require(['core/util/randomString'], function (randomString) {
        console.log(randomString() + ' is a random string'); // "1n0mr3 is a random string"
    })
}());
```

*/
/* jshint bitwise:false */
(function(define, Math){ "use strict";
	define([], function () {
        var generate = function(){
            return ((Math.random() * 0xfffffff )|0).toString(36);
        };
		return generate;
	});
})(define, Math);