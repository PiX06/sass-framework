'use strict';

module.exports = function(grunt) {

    /**
    * Grunt project config
    * Go to the bottom of this file for the configurable elements
    * */

 	var

        /**
         * Shortcut to grunt.file.readJSON(filename);
         * @param filename
         * @returns JS Object parsed from json at the supplied filename
         */
		readJSON = function(filename){
            return grunt.file.readJSON(filename);
        },

        /**
         * Shortcut to grunt.file.copy(fromFile, toFile);
         * @param fromFile
         * @param toFile
         * @returns {*}
         */
        copyFile = function(fromFile, toFile){
            return grunt.file.copy(fromFile, toFile);
        },
               
        

        /**
         * @param moduleName
         * @param path
         * @param contents
         * @param debug
         * @returns {*}
         */
		replaceBuildConstants = function (moduleName, path, contents, debug) {
			var output = contents;

			if(moduleName == 'app/build-info' && contents.indexOf('FRAMEWORK_NAME') != -1){

				if(!debug){ // A release build? increment build counter
					projectInfo.build++;
					grunt.file.write('project-info.json', JSON.stringify(projectInfo, null, '\t'));
				}

				output = output.replace(/FRAMEWORK_NAME/g, pkg.name);
				output = output.replace(/FRAMEWORK_VERSION/g, pkg.version);
				output = output.replace(/PROJECT_NAME/g, projectInfo.name);
				output = output.replace(/PROJECT_VERSION/g, projectInfo.version + ' r' + projectInfo.build);
				output = output.replace(/PROJECT_DESCRIPTION/g, projectInfo.description);
				output = output.replace(/PROJECT_BUILD_TIME/g, new Date().toString());
				output = output.replace(/"DEBUG"/g, debug+'');

			}

			return output;
		},


        /**
         * Load grunt config JSON files and set the loaded json objects as properties on the target object...
         *
         * @param target        Target object to add config-objects to
         * @param configList    Array of strings - the names of config files to load
         * @param [path]        Optional, defaults to 'grunt-config/'
         */
        loadConfigs = function(target, configList, path) {
            var base = typeof  path === 'undefined' ? 'grunt-config/' : path;

            var name, i, n = configList.length;
            for(i=0;i<n;i++){
                name         = configList[i];
                target[name] = readJSON(base + name + '.json');
            }
        },


		// grunt package info (used in replaceBuildConstants and the main grunt config)
		pkg = readJSON('package.json'),


		// project name, version, build info (used in replaceBuildConstants and the main grunt config)
		projectInfo = readJSON('project-info.json'),


        // grunt config object... Additional config objects will be added to this from the projectTasksConfig list
		config = { pkg : pkg },

        /**
        * Dynamically load npm tasks.
        */
        loadNPMTasks = function(){
            require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
        },

        /**
         * @private
         * initialise the defined Grunt tasks for the project
         */
        init = function(){

            loadConfigs(config, projectTasksConfig);

			// Add callbacks to the requirejs build writer to replace placeholders with values...
			config.requirejs['default'].options.onBuildWrite = function(moduleName, path, contents) { return replaceBuildConstants(moduleName, path, contents, true);};
			config.requirejs['dev'].options.onBuildWrite = function(moduleName, path, contents) { return replaceBuildConstants(moduleName, path, contents, true); };
			config.requirejs['dist'].options.onBuildWrite = function(moduleName, path, contents) { return replaceBuildConstants(moduleName, path, contents, false); };
            
            // init Grunt project configuration
            grunt.config.init(config);

            // load npm tasks
            loadNPMTasks();

            // define the available build tasks
            registerProjectBuildTasks();
        },




        // -------------------------------------------------------------------------------------------------------------
        // -------------------------------------------------------------------------------------------------------------
        // Configurable stuff starts here...


        // Grunt json config files to load (from /grunt-config/)
        // Files in the config dir should be named after the task being defined...
        projectTasksConfig = [

            "shell",        /* Run simple shell commands */

            "project",      /* Set project info and paths */

            "sass",         /* Libsass tasks */
            "bless",
            "sprite",
            "phantomcss",
            "scsslint",     /* Options: .scss-lint.yml */
            "svg2png",
            "imagemin",     /* optimise jpg, gif, png, svg */
            "parker",
            "sassdoc",

            "modernizr",    /* modernizr custom-build */
            "requirejs",    /* requirejs build config -  https://github.com/gruntjs/grunt-contrib-requirejs */
            "jshint",       /* lint the application modules prior to build */
            "qunit",        /* js unit tests */

            "watch",        /*  watch for changes, the default target... */
            "autoprefixer",
            "clean"         /* clean up util */
        ],
        

        /**
         * combine individual tasks to define the available build targets...
         */
        registerProjectBuildTasks = function(){
            
            // intall any ruby / bower dependencies
            grunt.registerTask('install', [
                'shell:bundler',
                'shell:bower'
            ]);

            // delete any previously built css + js files
            grunt.registerTask('clean-output', [
                'clean:css',
                'clean:js'
            ]);

            // default task is 'watch:all'
            grunt.registerTask('default', [ 'watch:all' ]);


            //
            // build outputs...
            grunt.registerTask('build:default', grunt.config.get('watch.all.tasks'));
            grunt.registerTask('build:dev', [
                'jshint',
                'image-processing',
                'sass:dev',
                'requirejs:dev',
                'modernizr'
            ]);
            grunt.registerTask('build:dist', [
                'clean-output',
                'js-tests',
                'image-processing',
                'sass-tests',
                'sass:dist',
                'requirejs:dist',
                'modernizr',
                'docs',
                'parker'
            ]);

                        
            // a function task... http://gruntjs.com/api/grunt.task
            grunt.registerTask('copyDocAssets', 'Copies the fframework readme.md to be used as the index.md for the generated documentation', function(){
                copyFile('../README.md', '../assets/sass/docs/styleguide/index.md');       
                copyFile('../build/hologram/_header.html', '../build/bower_components/Cortana/_header.html');       
                copyFile('../build/hologram/_footer.html', '../build/bower_components/Cortana/_footer.html');       
            });
           
            // documentation
            grunt.registerTask('docs', [
                'clean:docs',
                'copyDocAssets',
                'sassdoc',
                'shell:hologram'
            ]);


            //
            // tests...
            grunt.registerTask('js-tests', [
                'jshint',
                'qunit:all'
            ]);
            grunt.registerTask('sass-tests', [
                'scsslint'
            ]);

            grunt.registerTask('image-processing', [
                'svg2png',
                'sprite',
                'imagemin'
            ]);

            
        };


    /** load all the config and setup grunt... */
	init();
};
