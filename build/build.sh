#!/bin/bash

npm install

## NOTE: you only need to run the first `install` commands the first time you run a build, 
## or if the npm/grunt dependencies are changed/need updating.
## So comment them out once that's done, and your builds will be faster!

grunt build:dist
