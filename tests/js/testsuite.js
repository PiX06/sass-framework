(function () {
	
    // Configure RequireJS so it resolves relative module paths from the project js path.
    require.config({
        baseUrl: "../../assets/js",
    });
	
	// A list of all QUnit test Modules.  Make sure you include the `.js` 
	// extension so RequireJS resolves them as relative paths rather than
	// using the `baseUrl` value supplied above.
	var 	
		// Core libs
		coreModules = [
			"core/DeviceTests.js",
			"core/UtilTests.js"
		],
		
		// Add app modules here
		appModules = [
			"app/modules/ExampleTests.js"
		];
	
    // Resolve all coreModules and appModules, then start the Test Runner.
	require(coreModules.concat(appModules), function(){
		QUnit.load();
		QUnit.start();
	});
}());

