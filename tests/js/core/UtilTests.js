define(function (require) {

	var
		// import what we're testing...
		randomString = require('core/util/randomString'),

		// Helpers
		checkIsType = function(assert, typeStr, arr_ob){
			var n=arr_ob.length, i;
			for(i=0;i<n;i++){  assert.strictEqual(typeof arr_ob[i], typeStr); }
		};

	// Define the QUnit module and life-cycle.
	QUnit.module("core/Util", {
		setup: function () {
			
		},
		teardown: function () {

		}
	});


	//
	// Tests
	//
	// @see http://api.qunitjs.com/category/assert/
	//
	QUnit.test("Util - randomString", function (assert) {
        var r1 = randomString();
		assert.strictEqual(typeof r1, 'string');
		assert.strictEqual(typeof parseInt(r1, 36), 'number');
	});
});