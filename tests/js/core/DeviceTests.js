define(function (require) {

	var
		// import what we're testing...
		D = require('core/device'),

		// Helpers
		checkIsType = function(assert, typeStr, arr_ob){
			var n=arr_ob.length, i;
			for(i=0;i<n;i++){  assert.strictEqual(typeof arr_ob[i], typeStr); }
		};

	// Define the QUnit module and life-cycle.
	QUnit.module("core/Device", {
		setup: function () {
			D.start();
		},
		teardown: function () {

		}
	});



	//
	// Tests
	//
	// @see http://api.qunitjs.com/category/assert/
	//

	//QUnit.test("example - sanity check", function (assert) {
		//assert.ok(1===1, 'Just checking that everything is ok!');
	//});

	QUnit.test("Device - require import check", function (assert) {
		assert.strictEqual(typeof D, 'object');
		assert.notEqual(typeof D, 'undefined');
	});


    QUnit.test("Device - function properties", function (assert) {
        checkIsType(assert, "function", [
            D.start,
            D.isLandscape, D.isPortrait,
            D.getWindowWidth, D.getWindowHeight, D.getWindowAspectRatio
        ]);
	});


    QUnit.test("Device - boolean properties", function (assert) {
        checkIsType(assert, "boolean", [
            D.isAndroid, D.isiPod, D.isiPad, D.isiPhone,
            D.isiOS, D.isBlackberry, D.isKindle, D.isIEMobile,
			D.isIE, D.isChrome, D.isFirefox, D.isSafari,
            D.hasTouch, D.hasMobileUserAgent, D.isMobileOrTabletDevice,
            D.hasHTML5VideoCapability, D.isHighDPI
        ]);
	});


    QUnit.test("Device - number properties", function (assert) {
        checkIsType(assert, "number", [
            D.ieVersion,
            D.screenAspectRatio, D.screenWidth, D.screenHeight
        ]);
    });


    QUnit.test("Device - stringy properties", function (assert) {
        checkIsType(assert, "string", [
            D.osName
        ]);
    });

});